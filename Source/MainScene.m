#import "MainScene.h"
#import "GameScene.h"

@implementation MainScene

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    
    // The thing is, that if this fails, your app will 99.99% crash anyways, so why bother
    // Just make an assert, so that you can catch it in debug
    NSAssert(self, @"Whoops");
    
    CCNodeColor *backgroundColor = [CCNodeColor nodeWithColor:[CCColor whiteColor]];
    [self addChild:backgroundColor];
    
    CCSprite *backgroundSprite = [CCSprite spriteWithImageNamed:@"iTunesArtwork.png"];
    [backgroundSprite setTextureRect:CGRectMake(0, 0,640, 480)];
    backgroundSprite.contentSize = CGSizeMake(0.4f, 0.4f);
    backgroundSprite.position = CGPointZero;
    backgroundSprite.anchorPoint = CGPointZero;
    [self addChild:backgroundSprite];
    
    

//    // Background
//    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"ic_launcher.png"];
//    sprite.position = ccp(0.5, 0.5);
//    sprite.positionType = CCPositionTypeNormalized;
//    [self addChild:sprite];
    
    // The standard Hello World text
    CCLabelTTF *firstLabel = [CCLabelTTF labelWithString:@"My Hello World!" fontName:@"Times New Roman" fontSize:20];
    firstLabel.positionType = CCPositionTypeNormalized;
    firstLabel.position = CGPointMake(0.3f, 0.5f);
    firstLabel.color = [CCColor redColor];
    [self addChild:firstLabel];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Hello World" fontName:@"ArialMT" fontSize:16];
    label.positionType = CCPositionTypeNormalized;
    label.position = (CGPoint){0.5, 0.25};
    [self addChild:label];
    
    CCButton *button = [CCButton buttonWithTitle:@"[Start]" fontName:@"ArialMT" fontSize:40];
    button.color = [CCColor blackColor];
    [button setLabelColor:[CCColor lightGrayColor] forState:CCControlStateHighlighted];
    button.positionType = CCPositionTypeNormalized;
    button.position = ccp(0.5f,0.28f);
    [button setTarget:self selector:@selector(onStartClicked)];
    [self addChild:button];
    
    
    // done
    return self;
}

- (void) onStartClicked {
    NSLog(@"Start buttonClicked!");
    
    [[CCDirector sharedDirector] pushScene:[GameScene new] withTransition:[CCDefaultTransition transitionRevealWithDirection:CCTransitionDirectionLeft duration:0.5f]];
}
@end
