//
//  CCScene+GameScene.m
//  SandBox iOS
//
//  Created by Anvar Kayumov on 6/15/18.
//  Copyright © 2018 Apportable. All rights reserved.
//

#import "GameScene.h"
#import "AppDelegate.h"
#import <string.h>

@interface GameScene() {
    CCNode *gridNode;
    CCSprite *mainImage;
    CGPoint _lastDragPos;
    BOOL _isDragging;
    NSMutableArray *cellLightLayers;
    NSDictionary *imageData;
    CCScrollView *scrollView;
    //CCSpriteBatchNode *paintBatchNode;
}
@end

@implementation GameScene

- (id)init
{
    self = [super init];
    
    mainImage = [NSNull null];
    imageData = [NSNull null];
    
    gridNode = [CCNode node];
    gridNode.position = ccp(0,0);
    gridNode.scale = 0.5f;
    
    [self parseData];
    
    [self setupBackgrounds];
    [self setupImage];
    [self setupGrid];
    [self setupBacklightLayers];
    [self setupBottomPanel];
 
    [self addChild:gridNode];
    
    self.userInteractionEnabled  = YES;
    self.multipleTouchEnabled = YES;
    
    return self;
}

#pragma mark - Scroll background

- (void) scrollBackground : (CGFloat) delta
{
    gridNode.position = ccp(gridNode.position.x,gridNode.position.y+delta);
    
    if(gridNode.position.y>self.contentSize.height)
        gridNode.position = ccp(gridNode.position.x, 0);
}

#pragma mark - Update

- (void) update:(CCTime)delta
{
   // Do something every frame
}

#pragma mark - Setup

- (void) setupBackgrounds
{
    CCNodeColor *backgroundColor = [CCNodeColor nodeWithColor:[CCColor whiteColor]];
    backgroundColor.anchorPoint = ccp(0, 0);
    backgroundColor.scale = 2.0f;
    [self addChild:backgroundColor];
}

- (void) setupBottomPanel
{
    CCNode *content = [CCNode node];
    content.contentSizeType = CCSizeTypeNormalized;
    content.contentSize = CGSizeMake(2.0f, 0.25f);
    CCSpriteBatchNode *contentBatch = [CCSpriteBatchNode batchNodeWithFile:@"whiteCircle.png"];
    for(int i = 0; i < 10; i++)
    {
        CCSprite* contentItem = [CCSprite spriteWithTexture:contentBatch.texture];
        contentItem.color = [CCColor redColor];
        contentItem.position = ccp(i*50, contentItem.position.y);
        contentItem.scale = 0.2f;
        [contentBatch addChild:contentItem];
    }
    [content addChild:contentBatch];
    
    CCScrollView* scroll = [[CCScrollView alloc] initWithContentNode:content];
    scroll.horizontalScrollEnabled = YES;
    scroll.verticalScrollEnabled = NO;
    scroll.color = [CCColor redColor];
    scroll.contentSizeType = CCSizeTypePoints;
    scroll.contentSize = CGSizeMake(300,40);
    scroll.zOrder = 3;
    scroll.position = ccp(40, 0);

    scrollView = scroll;
    [self addChild:scrollView];
}

- (void) setupBacklightLayers
{
    if(imageData == (id)[NSNull null])
        return;
    
    cellLightLayers = [NSMutableArray array];
    NSArray *colorsData = [imageData objectForKey:@"colors"];
    
    float STEP = 20.48f;
    
    for (NSDictionary *color in colorsData)
    {
        CCSpriteBatchNode *layerBatchNode = [CCSpriteBatchNode batchNodeWithFile:@"img.png"];
        layerBatchNode.zOrder = 0;
       
        //NSString *colorHex = [color objectForKey:@"hex"];
        NSArray *positions = [color objectForKey:@"positions"];
        
        for(NSString *position in positions)
        {
            CCSprite *cellLight = [CCSprite spriteWithTexture:layerBatchNode.texture];
            
            float currentPosX, currentPosY;
            [self parseColorPosition:position indexX:&currentPosX indexY:&currentPosY];
           
            currentPosX*=STEP;
            currentPosY*=-STEP;
    
            CGPoint currentPoint = CGPointMake(currentPosX, currentPosY);
            cellLight.position = currentPoint;
            
            cellLight.color = [CCColor redColor];
            cellLight.scale = 0.64f;
            [layerBatchNode addChild:cellLight];
        }
        
        float anchorPointX = (-[mainImage boundingBox].size.width+20)/2;
        float anchorPointY = ([mainImage boundingBox].size.height-20)/2;
        layerBatchNode.position = ccp(anchorPointX, anchorPointY);
        [cellLightLayers addObject:layerBatchNode];
        [gridNode addChild:layerBatchNode];
    }
}

- (void) setupGrid
{
    if(mainImage == (id)[NSNull null])
       return;
    
    CCSprite *gridSprite = [CCSprite spriteWithImageNamed:@"img.png" ];
    
    const int GRID_DIMENSION    = 100;
    const int CELL_DIMENSION    = [gridSprite boundingBox].size.width;
    const int RECT_DIMENSION    = GRID_DIMENSION * CELL_DIMENSION;
    const int MAIN_IMAGE_WIDTH  = (int)[mainImage boundingBox].size.width;
    const int MAIN_IMAGE_HEIGHT = (int)[mainImage boundingBox].size.height;
    const int IMAGE_DIMENSION   = (MAIN_IMAGE_WIDTH > MAIN_IMAGE_HEIGHT) ? MAIN_IMAGE_WIDTH : MAIN_IMAGE_HEIGHT;
    
    const float GRID_SCALE = ((float)IMAGE_DIMENSION) / (float)RECT_DIMENSION;
    
    CGRect repeatRect = CGRectMake(-RECT_DIMENSION, -RECT_DIMENSION, RECT_DIMENSION, RECT_DIMENSION);
    gridSprite.textureRect = repeatRect;
    gridSprite.position = ccp(0, 0);
    gridSprite.scale = GRID_SCALE;
    gridSprite.zOrder = -1;
   
    ccTexParams texParams = {GL_REPEAT,GL_REPEAT,GL_REPEAT,GL_REPEAT};

    [gridSprite.texture setTexParameters:&texParams];
    [gridNode addChild:gridSprite];
}

- (void) setupImage
{
    CCSprite *imageSprite = [CCSprite spriteWithImageNamed:@"cow.png"];
    imageSprite.position = ccp(0, 0);
    imageSprite.scale = 2.0f;
    imageSprite.opacity = 0.5f;
    imageSprite.zOrder = 1;
    
    [gridNode addChild:imageSprite];
    mainImage = imageSprite;
}

#pragma mark - Touch handler

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    if([self isScaling:event])
    {
        mainImage.opacity = (1.0f-gridNode.scale)*1.2f;
        return;
    }
    
    if(_isDragging)
        [self drag:touch];
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [self setDragInitialPoint:touch];
    _isDragging = YES;
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    _isDragging = NO;
}

#pragma mark - Misc

-(void) setDragInitialPoint:(CCTouch *)touch
{
    CGPoint poNow = [touch locationInView:[touch view]];
    poNow = [[CCDirector sharedDirector] convertToGL:poNow];
    _lastDragPos = poNow;
}

- (void) drag:(CCTouch *)touch
{
    // Drag/swipe grid
    CGPoint poNow = [touch locationInView:[touch view]];
    poNow = [[CCDirector sharedDirector] convertToGL:poNow];
    
    CGPoint delta = ccp((poNow.x-_lastDragPos.x), (poNow.y - _lastDragPos.y));
    
    gridNode.position = ccpAdd(gridNode.position, delta);
    _lastDragPos = poNow;
    
    [[CCDirector sharedDirector].responderManager discardCurrentEvent];
}

//- (BOOL) isUITouched:
//{
//
//}

- (BOOL) isScaling:(CCTouchEvent *)event
{
    // Examine allTouches instead of just touches.  Touches tracks only the touch that is currently moving...
    // But stationary touches still trigger a multi-touch gesture.
    NSArray* allTouches = [[event allTouches] allObjects];
    if ([allTouches count] == 2)
    {
        // Get two of the touches to handle the zoom
        UITouch* touchOne = [allTouches objectAtIndex:0];
        UITouch* touchTwo = [allTouches objectAtIndex:1];
        
        // Get the touches and previous touches.
        CGPoint touchLocationOne = [touchOne locationInView: [touchOne view]];
        CGPoint touchLocationTwo = [touchTwo locationInView: [touchTwo view]];
        
        CGPoint previousLocationOne = [touchOne previousLocationInView: [touchOne view]];
        CGPoint previousLocationTwo = [touchTwo previousLocationInView: [touchTwo view]];
        
        // Get the distance for the current and previous touches.
        CGFloat currentDistance = sqrt(
                                       pow(touchLocationOne.x - touchLocationTwo.x, 2.0f) +
                                       pow(touchLocationOne.y - touchLocationTwo.y, 2.0f));
        
        CGFloat previousDistance = sqrt(
                                        pow(previousLocationOne.x - previousLocationTwo.x, 2.0f) +
                                        pow(previousLocationOne.y - previousLocationTwo.y, 2.0f));
        
        // Get the delta of the distances.
        CGFloat distanceDelta = currentDistance - previousDistance;
        
        // Next, position the camera to the middle of the pinch.
        // Get the middle position of the pinch.
        CGPoint pinchCenter = ccpMidpoint(touchLocationOne, touchLocationTwo);
        
        // Then, convert the screen position to node space... use your game layer to do this.
        pinchCenter = [gridNode convertToNodeSpace:pinchCenter];
        
        // Finally, call the scale method to scale by the distanceDelta, pass in the pinch center as well.
        // Also, multiply the delta by PINCH_ZOOM_MULTIPLIER to slow down the scale speed.
        // A PINCH_ZOOM_MULTIPLIER of 0.005f works for me, but experiment to find one that you like.
        [self scale:gridNode.scale + (distanceDelta * 0.0005f)
        scaleCenter:pinchCenter];
        
        return YES;
    }
    return NO;
}

- (void) scale:(CGFloat) newScale scaleCenter:(CGPoint) scaleCenter
{
    // scaleCenter is the point to zoom to..
    // If you are doing a pinch zoom, this should be the center of your pinch.
    
    // Get the original center point.
    CGPoint oldCenterPoint = ccp(scaleCenter.x * gridNode.scale, scaleCenter.y * gridNode.scale);
    
    // Set the scale.
    if(newScale>0.160f && newScale <1.0f)
         gridNode.scale = newScale;
    
    // Get the new center point.
    CGPoint newCenterPoint = ccp(scaleCenter.x * gridNode.scale, scaleCenter.y * gridNode.scale);
    
    // Then calculate the delta.
    CGPoint centerPointDelta  = ccpSub(oldCenterPoint, newCenterPoint);
    
    // Now adjust your layer by the delta.
    gridNode.position = ccpAdd(gridNode.position, centerPointDelta);
}

#pragma mark - Utils

-(CGPoint) locationFromTouches:(NSSet *)touches
{
    UITouch *touch = touches.anyObject;
    CGPoint touchLocation = [touch locationInView:touch.view];
    return [[CCDirector sharedDirector] convertToGL:touchLocation];
}

- (void) parseColorPosition:(NSString *)position indexX:(float *)x indexY:(float *)y
{
    NSArray *items = [position componentsSeparatedByString:@"."];
    *x = [[items objectAtIndex:0] floatValue];
    *y = [[items objectAtIndex:1] floatValue];
}

- (void) parseData
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"colors" ofType:@"json"];
    NSData *returnedData = [NSData dataWithContentsOfFile:path];
    
    if(NSClassFromString(@"NSJSONSerialization"))
    {
        NSError *error = nil;
        id object = [NSJSONSerialization
                     JSONObjectWithData:returnedData
                     options:0
                     error:&error];
        
        if(error) {}

        if([object isKindOfClass:[NSDictionary class]])
        {
            imageData = object;
        }
        else {}
    
    }
    else
    {
        // the user is using iOS 4; we'll need to use a third-party solution.
    }
}
@end
